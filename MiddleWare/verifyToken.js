const jwt = require('jsonwebtoken')
const config = require("../config")
module.exports = function (req, res, next) {
    // Get token from the header
    const token = req.header('x-auth-token')

    // check if token exists
    if (!token) {
        return res.status(401).json( "No token, access denied!!" )
    }

    jwt.verify(token, config.secret, (err, decoded) => {
        if (err) {
            res.status(401).json(   'Token not valid!' )
        }

        req.user = decoded.user
        next()
    })
}