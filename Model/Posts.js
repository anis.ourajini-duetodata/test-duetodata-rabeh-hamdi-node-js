const mongoose = require("mongoose");

const PostsSchema = new mongoose.Schema({
    Date: {
        type: Date,
        default: Date.now
    },
    Text: {
        type: String,
        require: true,
        min:8,
        max:16
    },
    Image: {
        type: String,
        require: true,
    },
    id_user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users'
    },
    like: {
        user_id: [{
            id_user: {
                type: String,
                require: true,
            },
            date: {
                type: Date,
                default: Date.now
            }
        }], 
        num: {
            type: Number,
            default : 0
            }
    },
    dislike: {
        user_id: [{
            id_user: {
                type: String,
                require: true,
            },
            date: {
                type: Date,
                default: Date.now
            }
        }],
        num: {
            type: Number,
            default: 0
        }
    },
    commantaire: [{
        Author: {
            type: String,
            require: true,
        },
        Text: {
            type: String,
            require: true,
        },
        Date: {
                type: Date,
                default: Date.now
            },
        Reaction:{
            type: String,
            require: true,
        },
        like: {
            user_id: [{
                id_user: {
                    type: String,
                    require: true,
                },
                date: {
                    type: Date,
                    default: Date.now
                }
            }], 
            num: {
                type: Number,
                default : 0
                }
        },
        dislike: {
            user_id: [{
                id_user: {
                    type: String,
                    require: true,
                },
                date: {
                    type: Date,
                    default: Date.now
                }
            }],
            num: {
                type: Number,
                default: 0
            }
        },
    }]
     
});
module.exports = mongoose.model("Posts", PostsSchema);