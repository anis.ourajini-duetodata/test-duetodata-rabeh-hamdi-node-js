const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
    Email: {
        type: String,
        require: true,
    },
    Password: {
        type: String,
        require: true,
        min:8,
        max:16
    },
    name: {
        type: String,
        require: true,
    },
    date: {
        type: Date,
        require: true,
    }
     
});
module.exports = mongoose.model("User", UserSchema);