const router = require('express').Router();
const User = require('../Model/User') ;
const bcrypt = require('bcryptjs');
const {registerValidation , loginValidation } = require('../validation_model/validation_User')
const config = require("../config")
const jwt = require("jsonwebtoken")


// Get All User
router.get('/allUser', async (req, res) => {
    const User = await User.find();
    res.send(User)
})

// Rgister  (Add User)
router.post('/addUser', async (req, res) => {

    const { error } = registerValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message)

    const E = await User.findOne({ Email: req.body.Email });
    if (E) return res.status(400).send('Your Email Exist !!!');

    const salt = bcrypt.genSaltSync(10);
    const hashPassword = await bcrypt.hash(req.body.Password,salt);
    const userr = new User({
        date:req.body.date,
        name: req.body.name,
        Email: req.body.Email,
        Password: hashPassword
    })
    
    userr.save()
        .then(r => res.json(r))
        .catch(err => {
            console.error(err.message)
            res.status(500).send("Server error")
        })
})

// Login
router.post("/login",async (req,res)=>{
    
    
    const { error } = loginValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message)
    
    
    const user = await User.findOne({Email:req.body.Email});
    if (!user) return res.status(400).send("Email is not found");

    const vaidPass = await bcrypt.compare(req.body.Password, user.Password);
    if (!vaidPass ) return res.status(400).send('Invalid password')

    const payload = {
        user: {
            id: user._id
        }
    } 
    const token = jwt.sign(payload, config.secret)
    res.json({ token }) 
})



module.exports = router;