const router = require('express').Router();
const Posts = require('../Model/Posts') ;
const config = require("../config") 
const multer = require('multer')
const path = require('path')
const auth = require('../MiddleWare/verifyToken')

// Uploade  image
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, path.join(__dirname, '../public/') )
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + "-" + file.originalname)
    }
})
const upload = multer({
    storage
})


// Delete commantaire de publication
// Private Route
router.get('/GetCommantaireByIdPosts/:idP', auth, (req, res) => {
    Posts.find({ _id: req.params.idP }).sort({ date: -1 })
    .then(Publication => res.json(Publication))
    .catch(err => {
        console.error(err.message)
        res.status(500).send('Server Error')
    })
})

// ADD Commantaire publication
// Private Route
router.put('/AddCommantaire/:id', auth, async(req, res) => {
    const { commantaire ,Reaction} = req.body 
    if (commantaire == undefined || commantaire.trim() === "") {
    res.json({ msg: "commantaire vide" })  }
    else if (Reaction == undefined || Reaction.trim() === "") {
    res.json({ msg: "Reaction vide" })  }
    else{ 

    await Posts.findById(req.params.id)
        .then(publication => {
            if (!publication) {
                return res.status(404).json({ msg:'publication not found'})
            }else {
                Posts.findByIdAndUpdate(req.params.id, { $push: { "commantaire": { "Author": req.user.id, "description": commantaire.trim(),"Reaction":Reaction } } }, (err, data) => {
                    
                }) 
                
            }
        }) 
        await Posts.findById(req.params.id)
            .then(p => { res.json({ msg: "Add Commantaire", publi: p }) })
            .catch(err => {
                console.error(err.message)
                res.status(500).send('Server error')
            })
    } 
})


// Delete commantaire de publication
// Private Route
router.delete('/DeleteCommantaire/:idP/:idC', auth, (req, res) => {
    Posts.findById(req.params.idP)
        .then(publication => {
            if (!publication) {
                return res.status(404).json({ msg:'publication not found'})
            } else {
                 
                Posts.findByIdAndUpdate(req.params.idP, { $pull: { commantaire: { _id: req.params.idC, Author:req.user.id} }}, (err, data) => {
                    res.json({ publi: data, idc: req.params.idC})
                })
            }
        })
        .catch(err => {
            console.error(err.message)
            res.status(500).send('Server error')
        })
})

// Update commantaire de publication
// Private Route
router.put('/UpdateCommantaire/:idP/:idC', auth, async(req, res) => {
    const { commantaire ,Reaction} = req.body 
    if (commantaire == undefined || commantaire.trim() === "") {
    res.json({ msg: "commantaire vide" })  }
    else if (Reaction == undefined || Reaction.trim() === "") {
    res.json({ msg: "Reaction vide" })  }
    else{  
        await Posts.findById(req.params.idP)
        .then(publication => {
            if (!publication) {
                return res.status(404).json({ msg: 'publication not found' })
            } else {

                Posts.findOneAndUpdate({ _id: req.params.idP }, { $pull: { commantaire: { _id: req.params.idC, Author: req.user.id }}  }, (err, data) => {
                    
                })
                Posts.findOneAndUpdate({ _id: req.params.idP }, { $push: { commantaire: { _id: req.params.idC, Author: req.user.id,  "description": commantaire.trim(),"Reaction":Reaction} } }, (err, data) => {
                     
                })
            }
        })
        .catch(err => {
            console.error(err.message)
            res.status(500).send('Server error')
        })
        await Posts.findById(req.params.idP)
            .then(p => { res.json({ msg: "Update Commantaire", publi: p, idc: req.params.idC, description: req.body.description }) })
            .catch(err => {
                console.error(err.message)
                res.status(500).send('Server error')
            })
    }
})




module.exports = router;