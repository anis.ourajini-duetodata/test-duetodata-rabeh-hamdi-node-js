const router = require('express').Router();
const Posts = require('../Model/Posts') ;
const config = require("../config") 
const multer = require('multer')
const path = require('path')
const auth = require('../MiddleWare/verifyToken')

// Uploade  image
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, path.join(__dirname, '../public/') )
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + "-" + file.originalname)
    }
})
const upload = multer({
    storage
})

// Get All Posts
router.get('/allPosts', async (req, res) => {
    const posts = await Posts.find();
    res.send(posts)
})

// Get My Publication
// Private Route
router.get('/getPublicationByAuth', auth, (req, res) => {
    Posts.find({ id_user: req.user.id }).sort({ date: -1 })
        .then(Publication => res.json(Publication))
        .catch(err => {
            console.error(err.message)
            res.status(500).send('Server Error')
        })
})

// Rgister  (Add User)
router.post('/addPosts', auth, upload.single('file'), async (req, res) => {
    var nameFile = ""
    if (req.file) { nameFile = req.file.filename}
    const posts = new Posts({
        Text:req.body.Text,
        Image: nameFile,
        id_user:req.user.id
    })
    
    posts.save()
        .then(r => res.json(r))
        .catch(err => {
            console.error(err.message)
            res.status(500).send("Server error")
        })
})


// Delete publication
// Private Route
router.delete('/DeleteById/:id', auth, (req, res) => {
    Posts.findById(req.params.id)
        .then(publication => {
            if (!publication) {
                return res.status(404).json( 'publication not found' )
            } else if (publication.id_user.toString() !== req.user.id) {
                res.status(401).json( "Not authorized" )
            } else {
                Posts.findByIdAndDelete(req.params.id, (err, data) => {
                    res.json("Publication Deleted!" )
                })
            }
        })
        .catch(err => {
            console.error(err.message)
            res.status(500).send('Server error')
        })
})


// Update publication
// Private Route
router.put('/UpdateByID/:id', auth, upload.single('file'), (req, res) => {
    const { Text } = req.body
    var nameFile = ""
    if (req.file) { nameFile = req.file.filename}
    
    let publicationFields = {}
    if (Text) publicationFields.Text = Text 
    if (nameFile) publicationFields.Image = nameFile  

    Posts.findById(req.params.id)
        .then(publication => { 
            if (!publication) {
                return res.status(404).json('publication not found' )
            } else if (publication.id_user.toString() !== req.user.id) {
                res.status(401).json(  "Not authorized" )
            } else {
                Posts.findByIdAndUpdate(req.params.id, { $set: publicationFields }, (err, data) => {
                    res.json(  "Updated publication!" )
                })
            }
        })
        .catch(err => {
            console.error(err.message)
            res.status(500).send('Server error')
        })
})




module.exports = router;