const express = require("express")
const app = express()
const mongoose = require("mongoose")
const config = require("./config")
const Auth = require('./Routes/Auth');
const Posts = require('./Routes/Posts');
const Commantaire = require('./Routes/Commantaire');
const Reactions_on_posts = require('./Routes/Reactions_on_posts');
const Reactions_on_comments = require('./Routes/reactions_on_comments');
mongoose.connect(
    "mongodb://localhost/DueToData",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    },
    (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log("Connected to MongoDB");
      }
    }
  );
app.use(express.json())
app.use("/api/Auth",Auth );
app.use("/api/Posts",Posts );
app.use("/api/Commantaire",Commantaire );
app.use("/api/ReactionsOnPosts",Reactions_on_posts );
app.use("/api/ReactionsOnComments",Reactions_on_comments );

app.get("/", (req, res) => {
    res.json({ message: "Welcome to bezkoder application." });
  });
  
app.listen(5000,()=>console.log("server is listening on port 5000"))