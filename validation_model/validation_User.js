const Joi = require('@hapi/joi');

 

//Register Validation
const registerValidation = data =>{
    const schema = {
        Email: Joi.string()
            .required()
            .email(),
        Password: Joi.string()
            .min(8)
            .max(16)
            .regex(/[A-Z]/, 'upper-case')
            .regex(/[a-z]/, 'lower-case')
            .regex(/[0-9]/, 'special character')
            .required(),
        name: Joi.string()
            .min(3)
            .required(),
        date: Joi.date()
            .required() 
             
    };
    return Joi.validate(data,schema);
};



const loginValidation = data => {
    const schema = {
        Email: Joi.string()
            .required()
            .email(),
        Password: Joi.string()
            .min(8)
            .max(16)
            .required(),
         
    };
    return Joi.validate(data, schema);
    
};

module.exports.registerValidation =  registerValidation;
module.exports.loginValidation = loginValidation;
